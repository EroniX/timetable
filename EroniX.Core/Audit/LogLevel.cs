﻿namespace EroniX.Core.Audit
{
    public enum LogLevel
    {
        Fatal,
        Error,
        Warning,
        Information,
        Debug,
        Trace
    }
}
