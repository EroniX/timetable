﻿using EroniX.Core.DataAccess;

namespace TimeTableDesigner.Shared.Access.UnitOfWork
{
    public interface ITimeTableUnitOfWorkFactory : IUnitOfWorkFactory<ITimeTableUnitOfWork>
    {
    }
}
