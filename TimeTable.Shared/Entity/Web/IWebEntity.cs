﻿namespace TimeTableDesigner.Shared.Entity.Web
{
    public interface IWebEntity
    {
        void Initialize(string[] values);
    }
}
